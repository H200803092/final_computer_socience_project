#!/bin/env python
"""
 * HYCU Gradution Portfolio - H200803092
 * torden@hycu.ac.kr
 *
 * @url : bitbucket.org/H200803092/final_computer_socience_project
 * @license : GPLv3, http://www.gnu.org/licenses/gpl.html
 * @author : torden cho <ioemen@gmail.com>
"""
from os import system
from sys import stdout

for i in range(1,100000):
    system("/usr/local/php-fpm/bin/php test.php > /dev/null 2>&1")
    stdout.write("\r")
    stdout.write("[%s] Test Data into Redis via php api" % i)
    stdout.flush()

