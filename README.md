#한양사비어대학교, H200803092 졸업프로젝트

 * goo.gl과 유사한 서비스를 졸업프로젝트로 작업합니다.
 * 긴 URL을 짧은 Code로 변환하여 제공하는 서비스로, 트위터와 같은 제한된 길이의 내용을 입력하는 서비스들에 사용되고 있음.
 * 리다이렉션은 nginx 모듈로 처리하여, 빠른 응답속도를 제공
 * 관리/운영용 웹페이지는 php로 개발
 * 데이타를 저장하는 디비는 Memory DB인 Redis를 제공하고, 시스템의 안정성을 위해, KyotoCabinet DBM도 이중으로 제공한다.
 * 이중지원하는 디비를 설정을 통해서 우선순위를 정할수 있도록 제공
 * phantom JS를 이용하여, 등록된 웹사이트에 대해서, screenshot을 제공한다.
 
# 졸업프로젝트 결과물

 * C로 개발된 nginx tdtinyurl module
 * C로 개발된 php extention module
 * C로 개발된 java (jni) extention module
 * Python과 Ruby/Java로 개발된 Redis Data to KyotoCabinet DBM 변환기
 * PHP / Jquery 로 작업된 운영/관리용 웹페이지
 * 설치 설명서 (MarkDown)
 * 구동 동영상 (Vimeo를 통해 제공예정)

# 동영상
 * 설치 동영상
[![ScreenShot](https://bytebucket.org/H200803092/final_computer_socience_project/raw/aa6d80ea6e4624c8126f622e62b9fae610932d80/tmp/vimeo-install-play.png?token=5650c7b2ba726cc26e46be59d47a3c9e85cdfac8)](http://youtu.be/He-s3CYnAFk)
 * 시연 동영상
[![ScreenShot](https://bytebucket.org/H200803092/final_computer_socience_project/raw/aa6d80ea6e4624c8126f622e62b9fae610932d80/tmp/vimeo-preview-play.png?token=94e8de9e605743045ea32b12c7a488b97317dcc9)](http://youtu.be/OPuRAfOBWa0)

# Requirement (Madantory)
 * Linux, gcc 3.x or Higher, gmake
 * Nginx (1.0.x or Higher) : http://nginx.org/en/download.html
 * Hiredis Latest version : https://github.com/redis/hiredis
 * KyotoCabinet (1.2.30 or higher) : http://fallabs.com/kyotocabinet/pkg/
 * Valgrind Latest : http://valgrind.org/
 * php 5.x or higher : http://kr1.php.net/downloads.php
 * Redis 2.0x or higher, *RECOMMENDED* 2.6.16 or Higher : http://download.redis.io/releases/redis-2.8.2.tar.gz
 * SQLite 3.0x or higher : http://www.sqlite.org/download.html
 * Phantomjs Latest verion (Embeded) : http://phantomjs.org/

# Requirement (Optional)
 * Python 2.3 or Higher : https://www.python.org/
 * Ruby 2.0x or Higher : https://www.ruby-lang.org/
 * JDK 1.6 or Higher : http://www.oracle.com/technetwork/java/index.html

# Support
 * Redis 데이타를 KyotoCabinet으로 변환하는 툴 : java, ruby, python
 * TinyURL 서비스를 할수 있는 Extention Module : php 5.x

# Data Structure

|Redis|
| ------------- | ------------- | ------------- |
|key|tinyurl|tduqid로 생성된 String을 저장합니다.|
|time|Expire Time|등록시간을 UNIXTIME으로 저장됩니다.|
|url|Long URL|원본 긴 URL|
|api|API Verion|PHP의 API버전이 올라갈때를 대비해서 저장됨. 차후를 위한 필드|
|secure|Secure Mode|0은 Normal Redirection, 1은 Web Page를 Fetch해서 보여줍니다.|
|rid|Related ID|관리툴이나 웹서비스를 구성할때 디비의 PRIMARY KEY정보를 넣어서, 차후 Relation을 유지하기 위함|

## Kyotocabinet DBM
 * Kyotocabinet DBM은 Redis 접속이나 서버자체의 문제가 발생시, Failover용으로 구성되어, Expire 기능은 제외했습니다.
 * 로그등으로 Redis 이상여부를 로깅합니다.
 * Redis To Kytocabinet DBM 스크립트를 Cron등을 통해서 지속적으로 만들어두는것이 좋습니다.

|Kyotocabinet DBM|
| ------------- | ------------- | ------------- |
|key|tinyurl|tduqid로 생성된 String을 저장합니다.|
|url|Long URL|원본 긴 URL|
|secure|Secure Mode|0은 Normal Redirection, 1은 Web Page를 Fetch해서 보여줍니다.|


## SQLITE
 * 웹서비스를 제공하기 위해서 사용함
 * TDTINYURL : 메인 테이블

|TDTINYURL|
| ------------- | ------------- | ------------- |
|rid|INTEGER|PRIMARY KEY AUTOINCREMENT|
|tinyurl|text|not null|
|longurl|text|not null|
|time|integer|not null|
|secure|integer|not null|
|hits|integer|not null|
|regdate|datetime|default current_timestamp|

 * TDTINYURL_LOG : 로깅

|TDTINYURL_LOG|
| ------------- | ------------- | ------------- |
|tinyurl|text|not null|
|agent|text|not null|
|ipaddr|text|not null|
|referer|text|
|regdate|datetime|default current_timestamp|
|FOREIGN KEY(tinyurl) REFERENCES TINYURL(tinyurl)|

# 설치 설명서

 * Prepared RPM packages
 * ZIP, PCRE , PYTHON , JDK , VALGRIND , OPENSSL
```
yum groupinstall "Develoment Tools" -y
yum install openssl.i686 openssl.x86_64 openssl-static.x86_64 openssl-devel.x86_64 openssl-devel.i686 -y
yum install valgrind glibc* -y
yum install zlib.i686 zlib.x86_64 zlib-devel.i686 zlib-devel.x86_64 zlib-static.x86_64 -y
yum install libxml2.i686 libxml2.x86_64 libxml2-devel.i686 libxml2-devel.x86_64 -y
yum install libcurl-devel.i686 libcurl-devel.x86_64 curl.x86_64 libcurl.i686 libcurl.x86_64 -y
yum install pcre-devel.i686 pcre-devel.x86_64 pcre-static.x86_64 pcre.i686 pcre.x86_64 -y
yum install python-devel.x86_64 python-devel.i686 -y
yum install java-1.7.0-openjdk-devel.x86_64 java-1.7.0-openjdk-src.x86_64 -y
yum install gcc gcc-g++* vim* -y

```


 * Install tduqid
 * Reason : TinyURL의 핵심이, Uniq Key Generator
```bash
git clone https://torden@bitbucket.org/torden/tduqid.git
cd tduqid
gmake
```

 * Install Hiredis
 * Reason : NGINX와 PHP Extention에서 사용하는 Library
```bash
git clone https://github.com/redis/hiredis
cd hiredis
PREFIX=/usr/local/hiredis gmake 
PREFIX=/usr/local/hiredis gmake install
```

 * Install KyotoCabinet
 * Reason : 백업또는 메인 저장소로 사용중인 KyotoCabinet DBM
```bash
wget http://fallabs.com/kyotocabinet/pkg/kyotocabinet-1.2.76.tar.gz
tar xvzf kyotocabinet-1.2.76.tar.gz
cd kyotocabinet-1.2.76
./configure --prefix=/usr/local/kyotocabinet --enable-static --enable-devel
gmake
gmake install
```

 * Install Nginx
 * Reason : TdTinyURL을 구동하기위한 웹서버인 NGINX
 * Comment : 최신 버전을 받아도 됩니다. 1.0.0 이상이면 정상동작합니다. 1.6.0까지 테스트 완료
```bash
mkdir /root/SRC/
cd /root/SRC/
wget http://nginx.org/download/nginx-1.5.7.tar.gz
tar xvzf nginx-1.5.7.tar.gz
```

 * Redis
 * Reason : 메인 저장소 또는 백업 저장소로 사용
```bash
cd /root/SRC/
wget http://download.redis.io/releases/redis-2.8.2.tar.gz
tar xvzf redis-2.8.2.tar.gz
cd redis-2.8.2
PREFIX=/usr/local/redis gmake
PREFIX=/usr/local/redis gmake install
```

 * SQLite3
 * Reason : 웹서비스를 위해 SQL을 지원하는 미니 디비가 필요해서 사용, 웹서비스를 별도 구축시에는 불필요.
```
cd /root/SRC/
wget http://www.sqlite.org/2014/sqlite-autoconf-3080403.tar.gz
tar xvzf sqlite-autoconf-3080403.tar.gz
cd sqlite-autoconf-3080403
./configure --prefix=/usr/local/sqlite3 --enable-threadsafe --enable-readline --enable-dynamic-extensions --with-pic --enable-static --enable-shared
gmake
gmake install
```

 * PHP(fpm mode)
 * Reason : 웹인터페이스를 구현한 언어
```bash
wget http://kr1.php.net/get/php-5.5.12.tar.gz/from/this/mirror
tar xvzf mirror
cd php-5.5.12

'./configure' \
'--prefix=/usr/local/php-fpm' \
'--enable-fpm' \
'--with-zlib=/usr/' \
'--enable-bcmath' \
'--with-gettext=/usr/' \
'--enable-mbstring' \
'--enable-sysvmsg' \
'--enable-sysvsem' \
'--enable-sysvshm' \
'--with-config-file-scan-dir=/usr/local/php-fpm/etc' \
'--with-config-file-path=/usr/local/php-fpm/etc' \
'--with-curl' \
'--enable-pcntl' \
'--disable-debug' \
'--enable-zip' \
'--enable-opcache' \

gmake
gmake install

cp -rf php.ini-production /usr/local/php-fpm/etc/php.ini
cp -rf sapi/fpm/php-fpm.conf /usr/local/php-fpm/etc/

/usr/local/php-fpm/sbin/php-fpm
```

 * phpRedis
 * Reason : PHP에서 Redis를 연결하기 위한, 별도구축시에는 불필요
```bash
git clone https://github.com/nicolasff/phpredis.git
cd phpredis/
/usr/local/php-fpm/bin/phpize
./configure --enable-redis --with-php-config=/usr/local/php-fpm/bin/php-config
gmake
gmake install

cp -rf rpm/redis.ini /usr/local/php-fpm/etc/
killall php-fpm
/usr/local/php-fpm/sbin/php-fpm

/usr/local/php-fpm/bin/php -i | fgrep Redis

Redis Support => enabled
Redis Version => 2.2.5
```

# Install Nginx tdtinyurl module
## regular
 * cd /root/SRC/nginx-1.5.*
 * ./configure --prefix=/usr/local/nginx/ --with-debug --add-module=TDTINYURL_CLONE_PATH/ngx_mod_tdtinyurl/
 * gmake
 * gmake install
 * tdtinyurl_nginx.conf content into /usr/local/nginx/conf/nginx.conf
 * restart nginx (killall nginx ; /usr/local/nginx/sbin/nginx)

## quick
 * TDTINYURL_CLONE_PATH/tdtinyurl_nginx.conf content into /usr/local/nginx/conf/nginx.conf
 * TDTINYURL_CLONE_PATH/ngx_mod_tdtinyurl/debug.fullcompile.sh

## Nginx Config directive
|directive|default flag|description|
| ------------- | ------------- | ------------- |
|tdtinyurl|off| on or off|
|tdtinyurl_work_mode|rl|rl is first lookup to redis and if redis failure, lookup to kyotocabinet dbm file|
|tdtinyurl_dbm_path|N/A|kyotocabinet dbm file full path|
|tdtinyurl_redis_connnect_mode|uds|uds or tcp|
|tdtinyurl_redis_uds_path|N/A|redis unix domain socket file path, default /tmp/redis.socket|
|tdtinyurl_redis_ip_addr|N/A|127.0.0.1 or localhost or xxxx.com|
|tdtinyurl_redis_port|6379|redis service port when set tcp mode to tdtinyurl_redis_connnect_mode|

 * sample
```config
[root@localhost ngx_mod_tdtinyurl]# cat tdtinyurl_nginx.conf
#nginx config
http {
    include             mime.types;
    default_type        application/octet-stream;
    sendfile            on;
    tcp_nopush          on;
    keepalive_timeout   65;
    server {
        listen          80;
        server_name     localhost;
        access_log      logs/host.access.log;

        # tdtinyurl
        location / {
            root                                html;
            index                               index.html index.htm;
            tdtinyurl                           on;
            tdtinyurl_work_mode                 "lr"; # lr is 1) Local DBM 2) Redis , rl = 1) Redis 2) Local
            tdtinyurl_dbm_path                  "/usr/local/data/tdtinyurl/tdtinyurl_kcdb.kch";
            tdtinyurl_redis_connnect_mode       uds ; # tcp or uds, recommended uds
            tdtinyurl_redis_uds_path            /tmp/redis.sock;
            tdtinyurl_redis_ip_addr             "127.0.0.1";
            tdtinyurl_redis_port                "6379";
        }

        # tdtinyurl raise below codes
        error_page   502 503 403 406  /fail.html;
        error_page   404 /not.html;
        error_page   500 /fail.html;
        error_page   504 /timeout.html;
        location = /not.html { root   html; }
        location = /fail.html { root   html; }
        location = /timeout.html { root   html; }
    }
}
```

## debugging

### nginx.conf
 * edit nginx.conf
 * error_log  logs/error.log debug;
 * restarting nginx

# Install Nginx tdtinyurl module
## regular
 * cd TDTINYURL_CLONE_PATH/php_tdtinyurl
 * PHP_HOME/bin/phpize
 * ./configure --with-php-config=PHP_HOME/bin/php-config --with-uqid=/usr/local/ --with-hiredis=/usr/local/hiredis/
 * gmake
 * gmake install
 * cp -rf tdtinyurl.ini PHP_HOME_CONF_PATH/tdtinyurl.ini
 * edit PHP_HOME_CONF_PATH/tdtinyurl.ini
 * restart web server
## quick
 * edit BUILD.sh
 * ./BUILD.sh

## testing
 * PHP_HOME/bin/php ./test.php
```
[root@localhost php_tdtinyurl]# for i in 1 2 3
> do
> /usr/local/php-fpm/bin/php ./test.php
> done

tdtinyurl_set_url : xrizSizShyPhyPgxOgxO
tdtinyurl_get_url : http://www.yahoo.com
tdtinyurl_set_rid : 1
tdtinyurl_get_rid : 1386096535

tdtinyurl_set_url : aS408b09beDjL2jK1iK1
tdtinyurl_get_url : http://www.yahoo.com
tdtinyurl_set_rid : 1
tdtinyurl_get_rid : 1386096535

tdtinyurl_set_url : Mj656df79azH6nG6nE5m
tdtinyurl_get_url : http://www.yahoo.com
tdtinyurl_set_rid : 1
tdtinyurl_get_rid : 1386096535
```

# api information

## tdtinyurl_set_url

```php
url into redis and return tinyurl key string

sting tdtinyurl_set_url(string $sURL, [cActionMode = TDTINYURL_LIVE, int $nExpireTime = -1, cWorkingMode = TDTINYURL_MODE_DEFAULT_REDIRECTION])
$sURL: Url ex) http://www.yahoo.com


cActionMode : TDTINYURL_LIVE is working mode and default action mode value, TDTINYURL_GET_ONLY_KEY is get only tinyurl key

$nExpireTime : expire time, if set this argument, this record will be destroy after (current time + your into expire time)m

cWorkingMode : TDTINYURL_MODE_DEFAULT_REDIRECTION is default tinyurl service mode , TDTINYURL_MODE_SECUER_TUNNELING is web page fetching and printing mode

```

```php
<?php
$sKey = tdtinyurl_set_url($sUrl);
print 'tdtinyurl_set_url : '. $sKey ."\n";
?>
```

## tdtinyurl_get_url
 * get url by tinyurl key string
```php
<?php
$sUrl = tdtinyurl_get_url($sKey);
print 'tdtinyurl_get_url : '. $sUrl."\n";
?>
```

## tdtinyurl_set_rid
 * set relation id to redis , related is means database primary key or uniq id

```php
<?php
$mRet = tdtinyurl_set_rid($sKey, time());
print 'tdtinyurl_set_rid : '. $mRet."\n";
?>
```

## tdtinyurl_get_rid
 * get tinyurl key string by url

```php
<?php
$sRid = tdtinyurl_get_rid($sKey);
print 'tdtinyurl_get_rid : '. $sRid ."\n";
?>
```