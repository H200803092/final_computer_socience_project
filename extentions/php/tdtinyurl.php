<?php
/**
 * HYCU Gradution Portfolio - H200803092
 * torden@hycu.ac.kr
 *
 * @url : bitbucket.org/H200803092/final_computer_socience_project
 * @license : GPLv3, http://www.gnu.org/licenses/gpl.html
 * @author : torden cho <ioemen@gmail.com>
 */
$br = (php_sapi_name() == "cli")? "":"<br>";

if(!extension_loaded('tdtinyurl')) {
	dl('tdtinyurl.' . PHP_SHLIB_SUFFIX);
}
$module = 'tdtinyurl';
$functions = get_extension_funcs($module);
echo "Functions available in the test extension:$br\n";
foreach($functions as $func) {
    echo $func."$br\n";
}
echo "$br\n";
$function = 'confirm_' . $module . '_compiled';
if (extension_loaded($module)) {
	$str = $function($module);
} else {
	$str = "Module $module is not compiled into PHP";
}
echo "$str\n";
?>
