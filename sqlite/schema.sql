CREATE TABLE TDTINYURL (
rid INTEGER PRIMARY KEY AUTOINCREMENT,
tinyurl text not null,
longurl text not null,
time integer not null,
secure integer not null,
hits integer not null,
regdate datetime default current_timestamp,
unique (tinyurl)
);

CREATE TABLE TDTINYURL_LOG (
tinyurl text not null,
agent text not null,
ipaddr text not null,
referer text,
regdate datetime default current_timestamp,
FOREIGN KEY(tinyurl) REFERENCES TINYURL(tinyurl)
);
